class WeatherWidget{

    getWeather(days) {
        return fetch('https://api.openweathermap.org/data/2.5/' + days + '?lang=ru&q=Minsk,by&appid=25bb06ade9e8fdcc8f801652ce5d1e1a')
            .then(function (resp) { return resp.json() });
    };

    async oneDayWeather() {
        const data = await this.getWeather('weather');
        //console.log(data)
        let celcius = Math.round(parseFloat(data.main.temp) - 273.15);
        document.getElementById('temp').innerHTML = celcius + '&deg;';
        document.getElementById('description').innerHTML = data.weather[0].description;
        document.getElementById('location').innerHTML = `сейчас в ${data.name}`;
        document.getElementById("wind").innerHTML = `ветер ${data.wind.speed} м/c`;
        document.getElementById("icon").innerHTML = `<img src="https://openweathermap.org/img/wn/${data.weather[0]['icon']}@2x.png">`;
    }
 
    async getThreeDaysWeather() {
        const data = await this.getWeather('forecast')
        const overalArea = document.querySelector(".threeDaysArea")
        let counter = 0
        for(let i=0;i<data.list.length;i++){
            if(data.list[i].dt_txt.includes('12:00:00')){
                const newArea =  document.createElement('div');
                newArea.className = "overal";
                const icon = data.list[i].weather[0]['icon']
                newArea.innerHTML = `
                <div>${Math.round(data.list[i].main.temp - 273) + '&deg;'}</div>
                <div>${data.list[i].weather[0]['description']}</div>
                <div>${Math.round(data.list[i].wind.speed) + 'М/с'}</div>
                <div>${`<img src="https://openweathermap.org/img/wn/${icon}@2x.png">`}</div>`;
                overalArea.appendChild(newArea);
                counter++;
                console.log(data.list[i]);
            }
            if(counter==3){
                break;
            }
        }
    }
}

const newWidget = new WeatherWidget()

const showOne = document.getElementById("sh1");
showOne.addEventListener("click", () => {
    newWidget.oneDayWeather();
})

const showThree = document.getElementById("sh3");
showThree.addEventListener("click", () => {
    newWidget.getThreeDaysWeather();
})

//понимаю, что не самые лучшие имена переменных
const closeAll = document.getElementById("close");
const des = document.getElementById("description");
const ic = document.getElementById("icon");
const te = document.getElementById("temp");
const lo = document.getElementById("location");
const wi = document.getElementById("wind");
const three = document.getElementById("test");

closeAll.addEventListener("click", ()=>{
    des.innerHTML = "";
    ic.innerHTML = "";
    te.innerHTML = "";
    lo.innerHTML = "";
    wi.innerHTML = "";
    three.innerHTML = "";
})

export default WeatherWidget;